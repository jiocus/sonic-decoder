![sonic-decoder](https://bitbucket.org/jiocus/sonic-decoder/src/master/assets/sonic-decoder.webp)

**SonicWall®,** network appliances and firewalls allows administrators to export and restore device configuration files from devices. Backup files may be useful for deployment, quick recovery after failure or as a source of reference. With countless options in networking, security and as a favorite for deployment in enterprise environments - these configuration files can contain invaluable documentation and encoded knowledge about their deployment.

The exported configuration file – suffixed `.exp` – is base64 encoded with an obscure padding scheme. Easily recovered into plaintext using `sonic-decoder`.

### Why?

**Convenience:** No need to look up *mystery b64 padding rituals* time and again.

**Security:** Decode and work with *sensitive data* and *organisation secrets* contained in `.exp` files. 

> Do not upload `.exp` files to third-party hosted web tools claiming to decode the content for you.

---



- Python script: 
    - https://bitbucket.org/jiocus/sonic-decoder/src/master/
    - `git clone https://jiocus@bitbucket.org/jiocus/sonic-decoder.git`

- Compiled
    - Linux (x86-64): https://bitbucket.org/jiocus/sonic-decoder/downloads/
    - Windows (x86-64): https://bitbucket.org/jiocus/sonic-decoder/downloads/

> Compiled binaries created with `pyinstaller`

How to use

**Prerequisite:**

- [x] sonic-decoder script or executable
- [x] SonicWall configuration file on disk



Python

```python
python sonic-decoder path/to/Sonicwall.exp
```

Linux
```bash
./sonic-decoder path/to/Sonicwall.exp
```

Powershell
```powershell
.\testapp.exesonic-decoder.exe path\to\Sonicwall.exp
```

### Todo

- [ ] Add safe example .exp file(s)