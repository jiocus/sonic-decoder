def fn_in() -> str:

    import os
    import sys

    try:
        filename = sys.argv[1]
        if os.path.isfile(filename):
            return filename
        else:
            print(f"Error: {filename} could not be found.")
            sys.exit()

    except IndexError:
        print(
            f"""
    ┌────────────────────┐
    │ ERROR: no file specified for decoding  │
    ├────────────────────┤
    │ FIX: provide filename or path to file  │
    │                                        │
    │ > {sys.argv[0]} path/to/file.exp    │
    └────────────────────┘

                sonic-decoder v1.1
        """
        )



def open_fn(fn: str) -> str:
    with open(fn, "r") as fn_r:
        data = fn_r.read()
    return data


def write_fn(fn: str, data: str) -> None:
    with open(fn, "w") as fn_w:
        fn_w.write(data)



def decode_data(data: str) -> str:
    from base64 import b64decode

    data = data[:-2]
    data = b64decode(data, altchars=None)
    data = str(data).replace("&", "\n")
    return data



def filename_out(fn: str) -> str:
    if fn.endswith(".exp"):
        fn_out = f"{fn[:-3]}plain"
    else:
        fn_out = f"{fn}.plain"
    return fn_out



def main():
    fn: str = fn_in()
    enc_data = open_fn(fn)
    dec_data = decode_data(enc_data)
    fn_out = filename_out(fn)
    write_fn(fn_out, dec_data)



if __name__ == "__main__":
    main()
